package kafka

import (
	"gRESTpIO-Persister/env"
	"github.com/segmentio/kafka-go"
)

func Init(config *env.EnvConfig) *kafka.Reader {
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:   []string{config.KafkaUrl},
		Topic:     config.KafkaTopic,
		Partition: 1,
		MinBytes:  1,    // 1 KB
		MaxBytes:  10e6, // 10MB
	})
}
