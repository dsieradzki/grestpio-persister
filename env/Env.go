package env

import (
	"os"
)

type EnvConfig struct {
	KafkaUrl   string
	KafkaTopic string

	MySQLDataSource   string
	MySQLDatabaseName string
}

func LoadEnvConfig() EnvConfig {
	kafkaHost := os.Getenv("KAFKA_HOST")
	kafkaPort := os.Getenv("KAFKA_PORT")
	kafkaTopic := os.Getenv("KAFKA_TOPIC")

	mySQLHost := os.Getenv("MYSQL_HOST")
	mySQLPort := os.Getenv("MYSQL_PORT")
	mySQLDatabase := os.Getenv("MYSQL_DATABASE")
	mySQLUser := os.Getenv("MYSQL_USER")
	mySQLPass := os.Getenv("MYSQL_PASSWORD")

	if len(kafkaHost) == 0 {
		kafkaHost = "localhost"
	}
	if len(kafkaPort) == 0 {
		kafkaPort = "9092"
	}
	if len(kafkaTopic) == 0 {
		kafkaTopic = "grestpio-topic"
	}
	if len(mySQLHost) == 0 {
		mySQLHost = "localhost"
	}
	if len(mySQLPort) == 0 {
		mySQLPort = "3306"
	}
	if len(mySQLDatabase) == 0 {
		mySQLDatabase = "grestpio"
	}
	if len(mySQLUser) == 0 {
		mySQLUser = "root"
	}
	if len(mySQLPass) == 0 {
		mySQLPass = "toor"
	}

	return EnvConfig{
		KafkaUrl:   kafkaHost + ":" + kafkaPort,
		KafkaTopic: kafkaTopic,

		MySQLDataSource:   mySQLUser + ":" + mySQLPass + "@tcp(" + mySQLHost + ":" + mySQLPort + ")/",
		MySQLDatabaseName: mySQLDatabase,
	}
}
