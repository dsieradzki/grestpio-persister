# Table
```sql
create database grestpio;
use grestpio

create table events(
id int AUTO_INCREMENT, 
pin varchar(10) not null, 
state varchar(255) not null,
received_date timestamp,PRIMARY key(id))
```