package main

import (
	"context"
	"gRESTpIO-Persister/database"
	"gRESTpIO-Persister/env"
	kaf "gRESTpIO-Persister/kafka"
	"log"
)

func main() {

	envConfig := env.LoadEnvConfig()

	kafkaTopic := kaf.Init(&envConfig)
	db := database.Init(&envConfig)
	defer db.Close()

	log.Print("gRESTpIO Persister is ready for work")
	for {
		message, err := kafkaTopic.ReadMessage(context.Background())

		if err != nil {
			log.Fatal(err)
			break
		}

		db.SaveMessage(message.Value)
		err = kafkaTopic.CommitMessages(context.Background(), message)
		if err != nil {
			log.Fatal(err)
			break
		}
	}

}
