package database

import (
	"database/sql"
	"gRESTpIO-Persister/env"
	"gRESTpIO-Persister/model"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

type Database struct {
	connection *sql.DB
}

func Init(config *env.EnvConfig) *Database {
	checkDatabase(config)
	con, err := sql.Open("mysql", config.MySQLDataSource+config.MySQLDatabaseName)
	if err != nil {
		log.Fatal(err)
	}
	return &Database{
		connection: con,
	}
}

func (db *Database) SaveMessage(data []byte) {
	event := model.FormJSON(data)
	log.Print("Message is received - ", event)
	if event != nil {
		_, err := db.connection.Exec("INSERT INTO events(pin, state, received_date) VALUES(?, ?, now())", event.Pin, event.State)
		if err != nil {
			log.Fatal(err, " - Cannot save event")
		}
	}
}

func (db *Database) Close() {
	db.Close()
}

func checkDatabase(config *env.EnvConfig) {
	db, err := sql.Open("mysql", config.MySQLDataSource)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	result, err := db.Query(`
			SELECT SCHEMA_NAME
	  		FROM INFORMATION_SCHEMA.SCHEMATA
	 		WHERE SCHEMA_NAME = '` + config.MySQLDatabaseName + `'
		`)
	if err != nil {
		log.Fatal(err)
	}

	var res string
	result.Next()
	_ = result.Scan(&res)
	if res != config.MySQLDatabaseName {
		_, err = db.Exec("CREATE DATABASE " + config.MySQLDatabaseName)
		if err != nil {
			log.Fatal(err)
		}
	}

	_, err = db.Exec(`
		create table IF NOT EXISTS ` + config.MySQLDatabaseName + `.events(
			id int AUTO_INCREMENT,
			pin varchar(10) not null,
			state varchar(255) not null,
			received_date timestamp,PRIMARY key(id))
		`)

	if err != nil {
		log.Fatal(err)
	}

}
