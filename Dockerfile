FROM golang:1.12 as build

ENV GO111MODULE=on

WORKDIR /go/src/app
COPY . .

RUN go mod download
RUN CGO_ENABLED=0 go build


FROM alpine:latest as dist
WORKDIR /root/
COPY --from=build /go/src/app/gRESTpIO-Persister .
CMD ["./gRESTpIO-Persister"]